/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagnostico;

/**
 *
 * @author rodri
 */
public class Investigadores extends Persona{ //clase heredando de la clase padre Persona
    //Declaracion de variables
    private String fInvento;
    private String invento;
    //Declaracion de constructor para llenado de datos
    public Investigadores(String nombre, String aMaterno, String aPaterno,String fInvento, String invento) {
        super(nombre, aMaterno, aPaterno);
        this.fInvento = fInvento;
        this.invento = invento;
    }

    //Get y Set de cada uno de los atributos de la clase
    public String getfInvento() {
        return fInvento;
    }

    public void setfInvento(String fInvento) {
        this.fInvento = fInvento;
    }

    public String getInvento() {
        return invento;
    }

    public void setInvento(String invento) {
        this.invento = invento;
    }

    @Override
    public String toString() {
        return "Fecha Invento= " + fInvento + ", Invento= " + invento;
    }
    
}
